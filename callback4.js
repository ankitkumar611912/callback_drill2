const boardData = require('./boards.json')
const listData = require('./lists_1.json')
const cardData = require('./cards.json')
const solve = require('./callback1');
const getIdInfoFromList = require('./callback2');
const getCardData = require('./callback3');

function getThanosInfo(){
    let thanosId = "mcu453ed";
    solve(thanosId,boardData,(err,data)=>{
        if(err){
            console.log("ID not found")
        }else{
            console.log(data);
            let listId= "mcu453ed";
            getIdInfoFromList(listId,boardData,listData,(err,data)=>{
                if(err){
                    console.log("ID not found")
                }else{
                    console.log(data);
                    getCardData("qwsa221",cardData,(err,data) =>{
                        if(err){
                            console.log("ID not found")
                        }else{
                            console.log(data);
                        }
                    })
                }
            })
        }
    });
    
}

module.exports = getThanosInfo;