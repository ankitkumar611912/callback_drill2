const boardData = require('../boards.json');
const listData = require('../lists_1.json');
const getIdInfoFromList = require('../promise2');

let index = Math.floor(Math.random()*(boardData.length+1));
let id = boardData[index].id;

getIdInfoFromList(id,boardData,listData)
    .then((data) => {
        console.log(data);
    })
    .catch((err) =>{
        console.log(err.message);
    })
