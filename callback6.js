const boardData = require('./boards.json')
const listData = require('./lists_1.json')
const cardData = require('./cards.json')
const solve = require('./callback1');
const getIdInfoFromList = require('./callback2');
const getCardData = require('./callback3');

function getThanosAllListAndCardInfo(){
    let thanosId = "mcu453ed";
    solve(thanosId,boardData,(err,data)=>{
        if(err){
            console.log("ID not found")
        }else{
            console.log(data);
            getIdInfoFromList(thanosId,boardData,listData,(err,data)=>{
                if(err){
                    console.log("ID not found")
                }else{
                    console.log(data);
                    data.forEach((each) => {
                        getCardData(each.id, cardData,(err, data) => {
                            if (err) {
                                console.error(err);
                            } else {
                                console.log(data);
                            }
                        });
                    });                
                }
            })
        }
    });
}

module.exports = getThanosAllListAndCardInfo;