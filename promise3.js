
function getCardData(listId,cardData){
    return new Promise((resolve, reject) => {
        setTimeout(() =>{
            let requiredData = cardData[listId]
            if(requiredData){
                resolve(requiredData);
            }else{
                let error = new Error("error occurred in id");
                reject(error);
            }
        },3000)
    })
}

module.exports = getCardData;