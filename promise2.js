function getIdInfoFromList(id,boardData,listData){
    return new Promise((resolve,reject) =>{
        setTimeout(()=>{
            let requiredId = boardData.find((data) => data.id === id);
            if(requiredId){
                let requiredData = listData[id];
                if(requiredData){
                    resolve(requiredData)
                }else{
                    let error = new Error("error occurred in id");
                    reject(error);
                }
            }else{
                let error = new Error("error occurred in id");
                reject(error);
            }
        },3000);
    })
    
}

module.exports = getIdInfoFromList;