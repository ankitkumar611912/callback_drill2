/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const boardData = require('./boards.json')
const listData = require('./lists_1.json')
const cardData = require('./cards.json')
const solve = require('./callback1');
const getIdInfoFromList = require('./callback2');
const getCardData = require('./callback3');

function getThanosInfo(){
    let thanosId = "mcu453ed";
    solve(thanosId,boardData,(err,data)=>{
        if(err){
            console.log("ID not found")
        }else{
            console.log(data);
            let listId= "mcu453ed";
            getIdInfoFromList(listId,boardData,listData,(err,data)=>{
                if(err){
                    console.log("ID not found")
                }else{
                    console.log(data);
                    data.forEach((each) => {
                        if (each.name == "Mind" || each.name == "Space") {
                          getCardData(each.id, cardData,(err, data) => {
                            if (err) {
                              console.log(err);
                            } else {
                              console.log(data);
                            }
                          });
                        }
                      });                
                }
            })
        }
    });
}

module.exports = getThanosInfo;