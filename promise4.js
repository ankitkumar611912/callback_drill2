const boardData = require('./boards.json')
const listData = require('./lists_1.json')
const cardData = require('./cards.json')
const solve = require('./promise1');
const getIdInfoFromList = require('./promise2');
const getCardData = require('./promise3');

function getThanosInfo(){
    let thanosId = "mcu453ed";
    solve(thanosId,boardData)
        .then((data) =>{
            console.log("Thanos Data");
            console.log(data);
            return getIdInfoFromList(thanosId,boardData,listData);
        })
        .catch((err) => {
            console.log(err.message);
            return err;
        })
        .then((listData) => {
            console.log("Print list Data....")
            console.log(listData);
            let listId = listData.find((data) => data.name === 'Mind');
            listId = listId.id;
            return getCardData(listId,cardData)
        })
        .catch((err) =>{
            console.log(err.message);
            return err;
        })
        .then((cardData) =>{
            console.log("Printing Card Data....")
            console.log(cardData);
        })
        .catch((err) =>{
            console.log(err.message);
            return err;
        })
    
}

module.exports = getThanosInfo;