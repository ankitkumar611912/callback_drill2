/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

const listData = require('../lists_1.json');
const cardData = require('../cards.json');
const getCardData = require('../callback3');

try{
    let listId = "qwsa221";
    getCardData(listId,cardData,(err,data) => {
        if(err){
            console.log("ID not found")
        }else{
            console.log(data);
        }
    })
}catch(err){
    console.error(err);
}