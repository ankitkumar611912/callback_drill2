/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const boardData = require('../boards.json');
const listData = require('../lists_1.json');
const getIdInfoFromList = require('../callback2');

try{
    
    let index = Math.floor(Math.random()*boardData.length);
    let id = boardData[index].id;
    
    getIdInfoFromList(id,boardData,listData,(err,data) =>{
        if(err){
            console.log("Id not found");
        }else{
            console.log(data);
        }
    })
}catch(err){
    console.error(err);
}