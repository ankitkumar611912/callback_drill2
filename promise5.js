const boardData = require('./boards.json')
const listData = require('./lists_1.json')
const cardData = require('./cards.json')
const solve = require('./promise1');
const getIdInfoFromList = require('./promise2');
const getCardData = require('./promise3');

function getThanosMoreInfo(){
    let thanosId = "mcu453ed";
    solve(thanosId,boardData)
        .then((data) =>{
            console.log("Thanos Data");
            console.log(data);
            return getIdInfoFromList(thanosId,boardData,listData);
        })
        .catch((err) => {
            console.log(err.message);
            return err;
        })
        .then((listData) => {
            console.log("Print list Data....")
            console.log(listData);
            let listId = listData.filter((data) => data.name === 'Mind' || data.name === 'Space');
            console.log("Print Card Data....")
            listId.forEach((listId) => {
                const value = getCardData(listId.id,cardData);
                value.then((carddata) =>{
                    console.log(carddata);
                })
                .catch((err) =>{
                    console.log(err.message);
                })
            });
        })
        .catch((err) =>{
            console.log(err.message);
            return err;
        })    
}

module.exports = getThanosMoreInfo;