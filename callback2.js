
function getIdInfoFromList(id,boardData,listData,callbackFn){
    setTimeout(()=>{
        let requiredId = boardData.find((data) => data.id === id);
        if(requiredId){
            let requiredData = listData[id];
            if(requiredData){
                callbackFn(null,requiredData);
            }else{
                callbackFn(new Error());
            }
        }else{
            callbackFn(new Error());
        }
    },3000);
}

module.exports = getIdInfoFromList;