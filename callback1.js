/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

function solve(id,boardData,callbackFn){
    setTimeout(() =>{
        if(typeof(callbackFn) === 'function' && typeof(id) === 'string' && Array.isArray(boardData)){
            let requiredData = boardData.find((data) => data.id === id);
            if(requiredData){
                callbackFn(null,requiredData);
            }else{
                callbackFn(new Error("Id not found"));
            }
        }else{
            console.log("Pass valid arguement");
        }
    },2000);
}

module.exports = solve;