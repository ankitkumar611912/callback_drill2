function solve(id, boardData) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let requiredData = boardData.find((data) => data.id === id);
            if (requiredData) {
                resolve(requiredData);
            } else {
                let error = new Error("error occurred in id");
                reject(error);
            }
        }, 2000);
    });
}

module.exports = solve;
